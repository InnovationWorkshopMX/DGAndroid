package com.iw.dynamicglucose;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.iw.dynamicglucose.services.get_lifestyle.Answer;

import java.util.List;

/**
 * Created by IW on 07/02/2018.
 */
public class AdapterLifestyle extends BaseAdapter{
    private LayoutInflater inflator;
    private List<Answer> answers;
    private Integer counter;
    private Integer size;

    AdapterLifestyle(Context context, List<Answer> answers, Integer counter, Integer size){
        inflator = LayoutInflater.from(context);
        this.answers = answers;
        this.counter = counter;
        this.size = size;
    }

    @Override
    public int getCount() {
        return answers.size();
    }

    @Override
    public Object getItem(int i) {
        return answers.get(i);
    }

    @Override
    public long getItemId(int i) {
        return answers.get(i).getId();
    }
    @SuppressLint("ViewHolder")
    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        final Answer answer = answers.get(i);
        View lifestyle_view = inflator.inflate(R.layout.cell_lifestyle, (ViewGroup) viewGroup.getRootView(), false);
        TextView title = lifestyle_view.findViewById(R.id.txt_cell_lifestyle);
        title.setText(answer.getAnswer());
        /*if (counter.equals(size - 1) || counter.equals(size - 2)){
            FrameLayout.LayoutParams params = (FrameLayout.LayoutParams) lifestyle_view.getLayoutParams();
            params.height = 320;
            lifestyle_view.setLayoutParams(params);
        }*/
        return lifestyle_view;

    }
}
