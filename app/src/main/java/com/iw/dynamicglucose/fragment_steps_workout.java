package com.iw.dynamicglucose;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.iw.dynamicglucose.config.BaseUrl;
import com.iw.dynamicglucose.config.Service;
import com.iw.dynamicglucose.config.Session;
import com.iw.dynamicglucose.services.Services;
import com.iw.dynamicglucose.services.applogin.UserSession;
import com.iw.dynamicglucose.services.process_workout.ProcessWorkout;
import com.iw.dynamicglucose.services.view_progress_program.ViewProgramProgress;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class fragment_steps_workout extends Fragment {
    //layout vars
    private TextView title;
    private TextView time;
    private ImageButton rate1;
    private ImageButton rate2;
    private ImageButton rate3;
    private ImageButton rate4;
    private ImageButton rate5;
    private TextView lbl_rate;
    private ListView listaSteps;
    private Button markasdone;
    private String id_workout;
    private TextView txtDays;
    private ImageView back_button_meals;

    //config vars
    private BaseUrl baseUrl;
    private Session session;
    private UserSession user;
    private Services services;

    private MaterialDialog dialogDays;

    public static fragment_steps_workout newInstance(Bundle arguments){
        fragment_steps_workout f = new fragment_steps_workout();
        if(arguments != null){
            f.setArguments(arguments);
        }
        return f;
    }

    public fragment_steps_workout() {
        // Required empty public constructor
    }

    // TODO: Rename and change types and number of parameters
    public static fragment_steps_workout newInstance(String param1, String param2) {
        return new fragment_steps_workout();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        listaSteps = getView().findViewById(R.id.list_steps_step_workout);
        title = getView().findViewById(R.id.txt_title_step_workout);
        time = getView().findViewById(R.id.txt_time_step_workout);
        rate1 = getView().findViewById(R.id.btn_rate_1_step_workout);
        rate2 = getView().findViewById(R.id.btn_rate_2_step_workout);
        rate3 = getView().findViewById(R.id.btn_rate_3_step_workout);
        rate4 = getView().findViewById(R.id.btn_rate_4_step_workout);
        rate5 = getView().findViewById(R.id.btn_rate_5_step_workout);
        lbl_rate = getView().findViewById(R.id.txt_rate_step_workout);
        markasdone = getView().findViewById(R.id.btn_markDone_step_workout);
        back_button_meals = getView().findViewById(R.id.back_button_meals);
        txtDays = getView().findViewById(R.id.lbl_days);

        id_workout = getArguments().getString("id");

        // Assignment vars config
        Service service = new Service();
        services = service.getService();
        session = new Session(getContext());
        user = session.getSession();

        if(session.getCodeNotif() != 0){
            session.setCodeNotif(0);
        }

        loadProgramProgress();

        markasdone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                markAsDone(user.getId(), Integer.parseInt(id_workout));
            }
        });
        rate1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                rate(user.getId(), Integer.parseInt(id_workout), 1);
            }
        });
        rate2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                rate(user.getId(), Integer.parseInt(id_workout), 2);
            }
        });
        rate3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                rate(user.getId(), Integer.parseInt(id_workout), 3);
            }
        });
        rate4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                rate(user.getId(), Integer.parseInt(id_workout), 4);
            }
        });
        rate5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                rate(user.getId(), Integer.parseInt(id_workout), 5);
            }
        });
        loadWorkouts(Integer.parseInt(id_workout),user.getId());

        back_button_meals.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Fragment selectedFragment = null;
                selectedFragment = fragment_workouts.newInstance();
                FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.frame_layout, selectedFragment);
                transaction.commit();
            }
        });
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_fragment_steps_workout, container, false);
    }
    private void rate(Integer patient_id, Integer workout_id, final Integer rating){
        // Create the call of the service time to start
        Call<ResponseBody> call = services.set_rating_workout(patient_id,workout_id,rating);

        // Executing call of the service time to start
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                switch (response.code()) {
                    case 200:
                        rate1.setEnabled(false);
                        rate2.setEnabled(false);
                        rate3.setEnabled(false);
                        rate4.setEnabled(false);
                        rate5.setEnabled(false);
                        switch(rating){
                            case 1:{
                                lbl_rate.setText("1/5");
                                rate1.setImageResource(R.drawable.star_checked);
                                break;
                            }
                            case 2:{
                                lbl_rate.setText("2/5");
                                rate1.setImageResource(R.drawable.star_checked);
                                rate2.setImageResource(R.drawable.star_checked);
                                break;
                            }
                            case 3:{
                                lbl_rate.setText("3/5");
                                rate1.setImageResource(R.drawable.star_checked);
                                rate2.setImageResource(R.drawable.star_checked);
                                rate3.setImageResource(R.drawable.star_checked);
                                break;
                            }
                            case 4:{
                                lbl_rate.setText("4/5");
                                rate1.setImageResource(R.drawable.star_checked);
                                rate2.setImageResource(R.drawable.star_checked);
                                rate3.setImageResource(R.drawable.star_checked);
                                rate4.setImageResource(R.drawable.star_checked);
                                break;
                            }
                            case 5:{
                                lbl_rate.setText("5/5");
                                rate1.setImageResource(R.drawable.star_checked);
                                rate2.setImageResource(R.drawable.star_checked);
                                rate3.setImageResource(R.drawable.star_checked);
                                rate4.setImageResource(R.drawable.star_checked);
                                rate5.setImageResource(R.drawable.star_checked);
                                break;
                            }
                            default:{
                                break;
                            }
                        }
                        break;
                    default:
                        break;
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.e("errorCall", t.getMessage());
            }
        });
    }
    private void markAsDone(Integer patient_id, Integer workout_id){
        // Create the call of the service time to start
        Call<ResponseBody> call = services.set_progress_workout(patient_id,workout_id);

        // Executing call of the service time to start
        call.enqueue(new Callback<ResponseBody>() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                switch (response.code()) {
                    case 200:
                        markasdone.setBackground(getResources().getDrawable(R.drawable.blue_checked_rounded_button));
                        markasdone.setTextColor(Color.WHITE);
                        markasdone.setText("DONE");
                        markasdone.setEnabled(false);
                        break;
                    default:
                        break;
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.e("errorCall", t.getMessage());
            }
        });
    }
    private void loadWorkouts(Integer id_workout, Integer id_patient){
        // Create the call of the service time to start
        Call<ProcessWorkout> call = services.process_workout(id_workout, id_patient);

        // Executing call of the service time to start
        call.enqueue(new Callback<ProcessWorkout>() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onResponse(Call<ProcessWorkout> call, Response<ProcessWorkout> response) {
                switch (response.code()){
                    case 200:
                        // Getting data objects
                        ProcessWorkout data = response.body();
                        // Set value to service response to label text days
                        assert data != null;
                        if(data.getDone()){
                            markasdone.setBackground(getResources().getDrawable(R.drawable.blue_checked_rounded_button));
                            markasdone.setText("DONE");
                            markasdone.setTextColor(Color.WHITE);
                            markasdone.setEnabled(false);
                        }
                        listaSteps.setAdapter(new AdapterWorkoutSteps(getContext(), data.getSteps(), data.getDuration()));
                        title.setText(data.getTitle());
                        time.setText(data.getDuration());
                        lbl_rate.setText(data.getRating().toString() + "/5");
                        if(data.getRating() != 0){
                            rate1.setEnabled(false);
                            rate2.setEnabled(false);
                            rate3.setEnabled(false);
                            rate4.setEnabled(false);
                            rate5.setEnabled(false);
                        }
                        switch (data.getRating()){
                            case 0:{
                                break;
                            }
                            case 1:{
                                rate1.setImageResource(R.drawable.star_checked);
                                break;
                            }
                            case 2:{
                                rate1.setImageResource(R.drawable.star_checked);
                                rate2.setImageResource(R.drawable.star_checked);
                                break;
                            }
                            case 3:{
                                rate1.setImageResource(R.drawable.star_checked);
                                rate2.setImageResource(R.drawable.star_checked);
                                rate3.setImageResource(R.drawable.star_checked);
                                break;
                            }
                            case 4:{
                                rate1.setImageResource(R.drawable.star_checked);
                                rate2.setImageResource(R.drawable.star_checked);
                                rate3.setImageResource(R.drawable.star_checked);
                                rate4.setImageResource(R.drawable.star_checked);
                                break;
                            }
                            case 5:{
                                rate1.setImageResource(R.drawable.star_checked);
                                rate2.setImageResource(R.drawable.star_checked);
                                rate3.setImageResource(R.drawable.star_checked);
                                rate4.setImageResource(R.drawable.star_checked);
                                rate5.setImageResource(R.drawable.star_checked);
                                break;
                            }
                            default:{
                                break;
                            }
                        }
                        break;
                    default:

                        break;
                }
            }

            @Override
            public void onFailure(Call<ProcessWorkout> call, Throwable t) {
                Log.e("Error", "call: " + t.getMessage());
            }


        });

    }

    private void loadProgramProgress(){
        dialogDays = loader();
        dialogDays.show();
        // Create the call of the service view program progress
        Call<ViewProgramProgress> call = services.view_program_progress(user.getId());

        // Executing call of the service view program progress
        call.enqueue(new Callback<ViewProgramProgress>() {
            @Override
            public void onResponse(Call<ViewProgramProgress> call, Response<ViewProgramProgress> response) {
                switch (response.code()){
                    case 200:
                        ViewProgramProgress data = response.body();
                        assert data != null;
                        if(data.getIs_active()){
                            txtDays.setText(data.getProgress_program());
                        }else{
                            session.CloseSession();
                            Intent i = new Intent(getContext(), Login.class)
                                    .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                            startActivity(i);
                            getActivity().finish();
                        }

                        break;
                    default:
                        MaterialDialog.Builder dialog = createMessage();
                        dialog.build();
                        dialog.show();
                        break;
                }
                dialogDays.dismiss();
            }

            @Override
            public void onFailure(Call<ViewProgramProgress> call, Throwable t) {
                dialogDays.dismiss();
                MaterialDialog.Builder dialog = createMessage();
                dialog.build();
                dialog.show();
            }
        });
    }

    private MaterialDialog.Builder createMessage(){
        return new MaterialDialog.Builder(getContext())
                .title("Error")
                .content("Server not available, try again later")
                .positiveText("Accept");
    }

    private MaterialDialog loader(){
        MaterialDialog.Builder builder = new MaterialDialog.Builder(getContext())
                .content("Please wait...")
                .progress(true, 0);
        return builder.build();
    }
}
