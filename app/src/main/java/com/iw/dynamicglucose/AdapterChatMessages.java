package com.iw.dynamicglucose;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.iw.dynamicglucose.config.Session;
import com.iw.dynamicglucose.services.Members.User;
import com.iw.dynamicglucose.services.applogin.UserSession;
import com.iw.dynamicglucose.services.get_history_message.Message;
import com.squareup.picasso.Picasso;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by anavictoriafrias on 06/02/18.
 */

public class AdapterChatMessages extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    Context context;
    private List<Message> messages;
    private List<User> users;

    private UserSession userSession;

    AdapterChatMessages(Context context, List<Message> messages, List<User> users) {
        this.context = context;
        this.messages = messages;
        this.users = users;

        //initiate config vars
        Session session = new Session(context);
        userSession = session.getSession();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v1 = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.cell_sender, parent, false);
        View v2 = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.cell_receiver, parent, false);
        switch (viewType) {
            case 1: return new ViewHolder1(v1);
            case 2: return new ViewHolder2(v2);
            default: return new ViewHolder1(v1);
        }
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {
        Message message = messages.get(position);
        if(holder.getItemViewType() == 1){
            ViewHolder1 viewHolder1 = (ViewHolder1)holder;
            viewHolder1.username.setText("Me");
            viewHolder1.textMessage.setText(message.getMessage());
            viewHolder1.time.setText(message.getTime());
            if(!userSession.getAvatar().equals("")){
                Picasso.with(context).load(userSession.getAvatar()).into(viewHolder1.user_image);
            }
        }else{
            for(User user : users){
                if(user.getEmail().equals(message.getUser())){
                    ViewHolder2 viewHolder2 = (ViewHolder2)holder;
                    viewHolder2.username.setText(message.getUser());
                    viewHolder2.textMessage.setText(message.getMessage());
                    viewHolder2.time.setText(message.getTime());
                    if(!user.getAvatar().equals("")){
                        Picasso.with(context).load(user.getAvatar()).into(viewHolder2.user_image);
                    }
                }else{
                    ViewHolder2 viewHolder2 = (ViewHolder2)holder;
                    viewHolder2.username.setText(message.getUser());
                    viewHolder2.textMessage.setText(message.getMessage());
                    viewHolder2.time.setText(message.getTime());
                }
            }
        }
    }

    @Override
    public int getItemViewType(int position) {
        Integer typeMessage;
        if(messages.get(position).getEmail().equals(userSession.getEmail())){
            typeMessage = 1;
        }else{
            typeMessage = 2;
        }
        return typeMessage;
    }

    @Override
    public int getItemCount() {
        return messages.size();
    }

    public static class ViewHolder1 extends RecyclerView.ViewHolder{

        TextView username;
        TextView textMessage;
        TextView time;
        CircleImageView user_image;

        ViewHolder1(View itemView){
            super(itemView);
            username = itemView.findViewById(R.id.user_name_sender);
            textMessage = itemView.findViewById(R.id.text_message_sender);
            time = itemView.findViewById(R.id.time_message_sender);
            user_image = itemView.findViewById(R.id.user_chat_sender);
        }
    }

    public static class ViewHolder2 extends RecyclerView.ViewHolder {

        TextView username;
        TextView textMessage;
        TextView time;
        CircleImageView user_image;

        ViewHolder2(View itemView) {
            super(itemView);
            username = itemView.findViewById(R.id.user_name);
            textMessage = itemView.findViewById(R.id.text_message);
            time = itemView.findViewById(R.id.time_message);
            user_image = itemView.findViewById(R.id.user_chat);
        }
    }
}
