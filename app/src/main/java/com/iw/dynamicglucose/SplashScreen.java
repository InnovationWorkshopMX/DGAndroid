package com.iw.dynamicglucose;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.afollestad.materialdialogs.MaterialDialog;
import com.iw.dynamicglucose.config.OnboardingPositions;
import com.iw.dynamicglucose.config.Service;
import com.iw.dynamicglucose.config.Session;
import com.iw.dynamicglucose.services.Services;
import com.iw.dynamicglucose.services.applogin.UserSession;
import com.iw.dynamicglucose.services.get_initial_info.GetInitialInfo;
import com.iw.dynamicglucose.services.time_to_start.TimeToStart;


import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SplashScreen extends AppCompatActivity {

    private Services services;
    private Session session;
    private UserSession user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        getSupportActionBar().hide();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            hideVirtualButtons();
        }

        // Assignment vars config
        Service service = new Service();
        services = service.getService();
        session = new Session(getApplicationContext());
        user = session.getSession();

        // Validate the user session
        if(user.getId() != 0){
            redirectActivityService();
        }else{
            redirectToLogin();
        }


    }

    private void redirectToLogin() {
        Intent i = new Intent(getApplicationContext(),  WelcomeSlider.class)
                .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        startActivity(i);
        finish();
    }

    private void redirectActivityService() {
        // Create the call of the service view program progress
        Call<GetInitialInfo> call = services.get_initial_info(user.getId());

        // Executing call of the service view program progress
        call.enqueue(new Callback<GetInitialInfo>() {
            @Override
            public void onResponse(Call<GetInitialInfo> call, Response<GetInitialInfo> response) {
                switch(response.code()){
                    case 200:
                        GetInitialInfo data = response.body();
                        assert data != null;
                        redirectActivity(data);
                        break;

                    default:
                        session.CloseSession();
                        redirectToLogin();
                        break;
                }
            }

            @Override
            public void onFailure(Call<GetInitialInfo> call, Throwable t) {
                redirectToLogin();
            }
        });
    }

    private void redirectActivity(GetInitialInfo data) {
        if(!data.getProgramFinish()){
           if(data.getOnboardFinish() && Integer.parseInt(data.getTimeToStart()) <=  0){
               Intent i = new Intent(getApplicationContext(), Home.class)
                       .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
               startActivity(i);
               finish();
           }else{
               if(Integer.parseInt(data.getTimeToStart()) <= 0){
                   OnboardingPositions positionObject = new OnboardingPositions();
                   Object[] position = positionObject.getArray();
                   // redireccion a la posicion del onboarding
                   Intent i = new Intent(getApplicationContext(), (Class<?>) position[Integer.parseInt(data.getPosition())])
                           .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                   startActivity(i);
                   finish();
               }else{
                   if(data.getOnboardFinish()){
                       loadScreen();
                   }else{
                       OnboardingPositions positionObject = new OnboardingPositions();
                       Object[] position = positionObject.getArray();
                       // redireccion a la posicion del onboarding
                       Intent i = new Intent(getApplicationContext(), (Class<?>) position[Integer.parseInt(data.getPosition())])
                               .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                       startActivity(i);
                       finish();
                   }
               }
           }
        }else{
            // Redirecciona al de la muchacha asi \o/
            Intent i = new Intent(getApplicationContext(), FinishProgram.class)
                    .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
            startActivity(i);
            finish();
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    private void hideVirtualButtons() {
        getWindow().getDecorView().setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                        | View.SYSTEM_UI_FLAG_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
    }

    private void loadScreen(){
        // Create the call of the service time to start
        Call<TimeToStart> call = services.time_to_start(user.getId());

        // Executing call of the service time to start
        call.enqueue(new Callback<TimeToStart>() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onResponse(Call<TimeToStart> call, Response<TimeToStart> response) {
                switch (response.code()){
                    case 200:
                        if(response.body().getTimeToStart() > 7){
                            Intent i = new Intent(getApplicationContext(), StandBy.class)
                                    .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                            i.putExtra("days", response.body().getTimeToStart());
                            startActivity(i);
                            finish();
                        }else{
                            Intent i = new Intent(getApplicationContext(), WatingScreen.class)
                                    .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                            startActivity(i);
                            finish();
                        }
                        break;
                    default:
                        break;
                }
            }

            @Override
            public void onFailure(Call<TimeToStart> call, Throwable t) {
            }
        });
    }
}
