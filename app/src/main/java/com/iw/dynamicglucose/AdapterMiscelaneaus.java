package com.iw.dynamicglucose;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;


/**
 * Created by IW on 07/02/2018.
 */
public class AdapterMiscelaneaus extends BaseAdapter{
    private LayoutInflater inflator;

    AdapterMiscelaneaus(Context context){
        inflator = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return 2;
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }
    @SuppressLint({"SetTextI18n", "ViewHolder", "InflateParams"})
    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        View misc_view = inflator.inflate(R.layout.cell_lifestyle, null);
        TextView title = misc_view.findViewById(R.id.txt_cell_lifestyle);
        if(i == 0){
            title.setText("Yes");
        }else{
            title.setText("No");
        }

        return misc_view;
    }
}
