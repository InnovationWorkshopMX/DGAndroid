package com.iw.dynamicglucose;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class FinishProgram extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_finish_program);
        getSupportActionBar().hide();
    }
}
