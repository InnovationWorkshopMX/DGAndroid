package com.iw.dynamicglucose.ExtraObjects;

/**
 * Created by IW on 02/04/2018.
 */

public class ItemSingleMeals {
    private Integer id;
    private String type_food;

    public ItemSingleMeals(Integer id, String type_food) {
        this.id = id;
        this.type_food = type_food;
    }
}
