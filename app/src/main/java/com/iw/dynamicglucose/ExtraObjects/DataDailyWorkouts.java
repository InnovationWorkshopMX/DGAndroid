package com.iw.dynamicglucose.ExtraObjects;

import java.util.ArrayList;

/**
 * Created by IW on 19/02/2018.
 */

public class DataDailyWorkouts {
    private Integer patient_id;
    private ArrayList<Integer> workouts;
    private ArrayList<Integer> mindsets;

    public DataDailyWorkouts(Integer patient_id, ArrayList<Integer> workouts) {
        this.patient_id = patient_id;
        this.workouts = workouts;
        this.mindsets = new ArrayList<Integer>();
    }
}
