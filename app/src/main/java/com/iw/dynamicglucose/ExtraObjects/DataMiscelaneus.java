package com.iw.dynamicglucose.ExtraObjects;

import java.util.ArrayList;

/**
 * Created by IW on 15/02/2018.
 */

public class DataMiscelaneus {
    private Integer patient_id;
    private ArrayList<Integer> data;

    public DataMiscelaneus(Integer patient_id, ArrayList<Integer> data) {
        this.patient_id = patient_id;
        this.data = data;
    }
}
