package com.iw.dynamicglucose.ExtraObjects;

import com.iw.dynamicglucose.services.get_daily_check_meals.Option;

/**
 * Created by IW on 22/02/2018.
 */

public interface OnItemClick {
    void onItemClick(Option item, Integer position, Integer rPosition);
}
