package com.iw.dynamicglucose.ExtraObjects;

/**
 * Created by anavictoriafrias on 31/01/18.
 */

public class HomeMeal {
    private String current;
    private String timeMeal;
    private String meals;
    private String urlImageMeal;

    private Integer id;

    public HomeMeal(String current, String timeMeal, String meals, String urlImageMeal, Integer id) {
        this.current = current;
        this.timeMeal = timeMeal;
        this.meals = meals;
        this.urlImageMeal = urlImageMeal;
        this.id = id;
    }

    public HomeMeal() {
    }

    public String getCurrent() {
        return current;
    }

    public void setCurrent(String current) {
        this.current = current;
    }

    public String getTimeMeal() {
        return timeMeal;
    }

    public void setTimeMeal(String timeMeal) {
        this.timeMeal = timeMeal;
    }

    public String getMeals() {
        return meals;
    }

    public void setMeals(String meals) {
        this.meals = meals;
    }

    public String getUrlImageMeal() {
        return urlImageMeal;
    }

    public void setUrlImageMeal(String urlImageMeal) {
        this.urlImageMeal = urlImageMeal;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
}
