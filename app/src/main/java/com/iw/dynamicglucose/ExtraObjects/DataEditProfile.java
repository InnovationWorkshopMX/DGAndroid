package com.iw.dynamicglucose.ExtraObjects;

import java.util.ArrayList;
import java.util.Map;

/**
 * Created by IW on 15/02/2018.
 */

public class DataEditProfile {

    private Integer patient_id;
    private String full_name;
    private String birth_date;
    private String height;
    private String weight;
    private ArrayList<Map<String,String>> measures;

    public DataEditProfile(Integer patient_id, String full_name, String birth_date, String height, String weight, ArrayList<Map<String, String>> measures) {
        this.patient_id = patient_id;
        this.full_name = full_name;
        this.birth_date = birth_date;
        this.height = height;
        this.weight = weight;
        this.measures = measures;
    }
    public  DataEditProfile(){

    }

    public Integer getPatient_id() {
        return patient_id;
    }

    public void setPatient_id(Integer patient_id) {
        this.patient_id = patient_id;
    }

    public String getFull_name() {
        return full_name;
    }

    public void setFull_name(String full_name) {
        this.full_name = full_name;
    }

    public String getBirth_date() {
        return birth_date;
    }

    public void setBirth_date(String birth_date) {
        this.birth_date = birth_date;
    }

    public String getHeight() {
        return height;
    }

    public void setHeight(String height) {
        this.height = height;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

    public ArrayList<Map<String, String>> getMeasures() {
        return measures;
    }

    public void setMeasures(ArrayList<Map<String, String>> measures) {
        this.measures = measures;
    }
}
