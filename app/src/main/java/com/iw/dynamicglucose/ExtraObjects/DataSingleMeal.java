package com.iw.dynamicglucose.ExtraObjects;

import java.util.ArrayList;
import java.util.Map;

/**
 * Created by IW on 02/04/2018.
 */

public class DataSingleMeal {
    private Integer patient_id;
    private Integer type;
    private Integer meal_time;
    private ArrayList<ItemSingleMeals> foods_items;

    public DataSingleMeal(Integer patient_id, Integer type, Integer meal_time, ArrayList<ItemSingleMeals> foods_items) {
        this.patient_id = patient_id;
        this.type = type;
        this.meal_time = meal_time;
        this.foods_items = foods_items;
    }
}
