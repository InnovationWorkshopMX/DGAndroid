
package com.iw.dynamicglucose.services.get_history_message;

import java.util.List;

public class GetMessages {

    private List<Message> messages = null;
    private Integer lastId;

    public List<Message> getMessages() {
        return messages;
    }

    public void setMessages(List<Message> messages) {
        this.messages = messages;
    }

    public Integer getLastId() {
        return lastId;
    }

    public void setLastId(Integer lastId) {
        this.lastId = lastId;
    }

}
