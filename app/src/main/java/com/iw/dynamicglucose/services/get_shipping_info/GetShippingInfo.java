
package com.iw.dynamicglucose.services.get_shipping_info;


public class GetShippingInfo {

    private Integer res;
    private ShoppingAddress shoppingAddress;

    public Integer getRes() {
        return res;
    }

    public void setRes(Integer res) {
        this.res = res;
    }

    public ShoppingAddress getShoppingAddress() {
        return shoppingAddress;
    }

    public void setShoppingAddress(ShoppingAddress shoppingAddress) {
        this.shoppingAddress = shoppingAddress;
    }

}
