
package com.iw.dynamicglucose.services.get_lifestyle;

import java.util.List;

public class GetLifestyle {

    private String question;
    private List<Answer> answers = null;

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public List<Answer> getAnswers() {
        return answers;
    }

    public void setAnswers(List<Answer> answers) {
        this.answers = answers;
    }

}
