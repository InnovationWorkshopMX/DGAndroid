
package com.iw.dynamicglucose.services.get_weight;

import java.util.List;

public class GetWeight {

    private List<Weight> weights = null;

    public List<Weight> getWeights() {
        return weights;
    }

    public void setWeights(List<Weight> weights) {
        this.weights = weights;
    }

}
