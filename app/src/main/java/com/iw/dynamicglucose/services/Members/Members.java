
package com.iw.dynamicglucose.services.Members;

import java.util.List;

public class Members {

    private List<User> users = null;

    public List<User> getUsers() {
        return users;
    }

    public void setUsers(List<User> users) {
        this.users = users;
    }

}
