
package com.iw.dynamicglucose.services.workout_home;

import java.util.List;

public class TodayWorkouts {

    private List<WorkoutFir> workoutFir = null;
    private List<WorkoutSec> workoutSec = null;

    public List<WorkoutFir> getWorkoutFir() {
        return workoutFir;
    }

    public void setWorkoutFir(List<WorkoutFir> workoutFir) {
        this.workoutFir = workoutFir;
    }

    public List<WorkoutSec> getWorkoutSec() {
        return workoutSec;
    }

    public void setWorkoutSec(List<WorkoutSec> workoutSec) {
        this.workoutSec = workoutSec;
    }

}
