
package com.iw.dynamicglucose.services.get_day_meals;


public class GetDayMeal {

    private Integer recipeId;
    private Integer group;
    private Integer mealTimeId;
    private String mealTime;
    private String mealTimeName;
    private String image;
    private String message;
    private String mealTimeNameTime;

    public Integer getRecipeId() {
        return recipeId;
    }

    public void setRecipeId(Integer recipeId) {
        this.recipeId = recipeId;
    }

    public Integer getGroup() {
        return group;
    }

    public void setGroup(Integer group) {
        this.group = group;
    }

    public Integer getMealTimeId() {
        return mealTimeId;
    }

    public void setMealTimeId(Integer mealTimeId) {
        this.mealTimeId = mealTimeId;
    }

    public String getMealTime() {
        return mealTime;
    }

    public void setMealTime(String mealTime) {
        this.mealTime = mealTime;
    }

    public String getMealTimeName() {
        return mealTimeName;
    }

    public void setMealTimeName(String mealTimeName) {
        this.mealTimeName = mealTimeName;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getMealTimeNameTime() {
        return mealTimeNameTime;
    }

    public void setMealTimeNameTime(String mealTimeNameTime) {
        this.mealTimeNameTime = mealTimeNameTime;
    }

}
