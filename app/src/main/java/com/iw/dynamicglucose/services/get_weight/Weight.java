
package com.iw.dynamicglucose.services.get_weight;


public class Weight {

    private String date;
    private Double weight;

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public Double getWeight() {
        return weight;
    }

    public void setWeight(Double weight) {
        this.weight = weight;
    }

}
