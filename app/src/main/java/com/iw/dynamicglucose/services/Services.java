package com.iw.dynamicglucose.services;

import com.iw.dynamicglucose.DailyCheckWorkouts;
import com.iw.dynamicglucose.ExtraObjects.DataDailyMeals;
import com.iw.dynamicglucose.ExtraObjects.DataDailyWorkouts;
import com.iw.dynamicglucose.ExtraObjects.DataDontFollow;
import com.iw.dynamicglucose.ExtraObjects.DataEditProfile;
import com.iw.dynamicglucose.ExtraObjects.DataFeeling;
import com.iw.dynamicglucose.ExtraObjects.DataLifestyle;
import com.iw.dynamicglucose.ExtraObjects.DataMiscelaneus;
import com.iw.dynamicglucose.ExtraObjects.DataSingleMeal;
import com.iw.dynamicglucose.services.DailyWorkout.DailyWorkouts;
import com.iw.dynamicglucose.services.do_daily_check.DoDailyCheck;

import com.iw.dynamicglucose.ExtraObjects.Data;
import com.iw.dynamicglucose.services.Members.Members;
import com.iw.dynamicglucose.services.applogin.UserSession;
import com.iw.dynamicglucose.services.change_pass.UpdatePass;
import com.iw.dynamicglucose.services.digital_pairing.GetImei;
import com.iw.dynamicglucose.services.equipment.Equipment;
import com.iw.dynamicglucose.services.get_daily_check_meals.GetDailyCheckMeal;
import com.iw.dynamicglucose.services.get_day_meals.GetDayMeal;
import com.iw.dynamicglucose.services.get_day_mindsets_work.GetDayMindsetsWork;
import com.iw.dynamicglucose.services.get_food_items.FoodItem;
import com.iw.dynamicglucose.services.get_history_message.GetMessages;
import com.iw.dynamicglucose.services.get_initial_info.GetInitialInfo;
import com.iw.dynamicglucose.services.get_lifestyle.GetLifestyle;
import com.iw.dynamicglucose.services.get_shipping_info.GetShippingInfo;
import com.iw.dynamicglucose.services.get_single_recipe.GetSingleRecipe;
import com.iw.dynamicglucose.services.get_weight.GetWeight;
import com.iw.dynamicglucose.services.lifestyle_video.LifestyleVideo;
import com.iw.dynamicglucose.services.process_workout.ProcessWorkout;
import com.iw.dynamicglucose.services.single_meals.SingleMeals;
import com.iw.dynamicglucose.services.single_members.SingleMember;
import com.iw.dynamicglucose.services.time_to_start.TimeToStart;
import com.iw.dynamicglucose.services.today_meals_list.TodayMealsList;
import com.iw.dynamicglucose.services.today_mindset.TodayMindset;
import com.iw.dynamicglucose.services.today_process_mindset.TodayProcessMindset;
import com.iw.dynamicglucose.services.user_profile.UserProfile;
import com.iw.dynamicglucose.services.video_goal.VideoGoal;
import com.iw.dynamicglucose.services.view_onboarding_articles.ViewOnboardingArticles;
import com.iw.dynamicglucose.services.view_progress_program.ViewProgramProgress;
import com.iw.dynamicglucose.services.view_single_article.ViewSingleArticle;
import com.iw.dynamicglucose.services.workout_home.TodayWorkouts;

import java.util.ArrayList;
import java.util.Map;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.PartMap;
import retrofit2.http.Path;

/**
 * Created by IW on 29/ene/2018.
 */

public interface Services {

    // Service get initial info
    @GET("onboard/get_initial_info/{patient_id}/")
    Call<GetInitialInfo> get_initial_info(@Path("patient_id") Integer patient_id);

    // Service time to start
    @GET("programs/time_to_start/{patient_id}/")
    Call<TimeToStart> time_to_start(@Path("patient_id") Integer patient_id);

    // Service home workouts
    @GET("workouts/today_workouts/{patient_id}/")
    Call<TodayWorkouts> today_workouts(@Path("patient_id") Integer patient_id);

    // Service get process workout
    @GET("workouts/get_process_workout/{workout_id}/{user_id}/")
    Call<ProcessWorkout> process_workout(@Path("workout_id") Integer workout_id, @Path("user_id") Integer user_id);

    //Service home meals
    @GET("foods/get_day_meals/{user_id}/")
    Call<ArrayList<GetDayMeal>> get_day_meals(@Path("user_id") Integer user_id);

    // Service home (get_day_mindsets_work)
    @GET("mindsets/get_day_mindsets_work_android/{user_id}/")
    Call<GetDayMindsetsWork> get_day_mindsets_work(@Path("user_id") Integer user_id);

    //Service Login
    @POST("applogin/")
    @FormUrlEncoded
    Call<UserSession> login(@Field("email") String email,
                            @Field("password") String password);


    //Service get progress
    @GET("api/users/get_weight/{patient_id}/")
    Call<GetWeight> get_weight(@Path("patient_id") Integer patient_id);

    // Service home mindset
    @GET("mindsets/today_mindsets/{patient_id}/")
    Call<ArrayList<TodayMindset>> today_mindset(@Path("patient_id") Integer patient_id);

    // Service home process mindset
    @GET("mindsets/get_single_mindset/{id_mindset}/{patient_id}/")
    Call<TodayProcessMindset> today_process_mindset(@Path("id_mindset") Integer id_mindset, @Path("patient_id") Integer patient_id);

    // Service view progress program
    @GET("users/view_progress_program/{user_id}/")
    Call<ViewProgramProgress> view_program_progress(@Path("user_id") Integer id_user);

    // Service get foodItems
    @GET("foods/get_food_items_android/{id}")
    Call<ArrayList<FoodItem>> get_food_items(@Path("id") String id);

    // Service get members
    @GET("users/view_members/{id}")
    Call<Members> get_members(@Path("id") String id);

    // Service get single member
    @GET("users/view_single_member/{id}")
    Call<SingleMember> get_single_member(@Path("id") String id);

    // Service today meals list
    @GET("foods/today_meals_list/{patient_id}/")
    Call<ArrayList<TodayMealsList>> today_meals_list(@Path("patient_id") Integer patient_id);

    // Service get single recipe
    @GET("foods/get_single_recipe/{recipe_id}/{patient_id}/")
    Call<GetSingleRecipe> get_single_recipe(@Path("recipe_id") String recipe_id,
                                            @Path("patient_id") Integer patient_id);

    //Service register progress day food
    @POST("progress/register_progress_day_food/")
    @FormUrlEncoded
    Call<ResponseBody> register_progress_day_food(@Field("type") Integer type,
                                                  @Field("meal_time") Integer meal_time,
                                                  @Field("patient_id") Integer patient_id,
                                                  @Field("food_id") Integer food_id);

    //Service register progress day food
    @POST("progress/register_progress_day_food/")
    Call<ResponseBody> register_progress_day_food_items(@Body DataSingleMeal data);

    //Service progress workout
    @POST("progress/register_progress_workout/")
    @FormUrlEncoded
    Call<ResponseBody> set_progress_workout(@Field("patient_id") Integer patient_id,
                                            @Field("workout_id") Integer workout_id);
    //Service progress workout
    @POST("progress/register_progress_mindset/")
    @FormUrlEncoded
    Call<ResponseBody> set_progress_mindset(@Field("patient_id") Integer patient_id,
                                            @Field("mindset_id") Integer mindset_id);

    //Service rate workout
    @POST("workouts/rating_workout/")
    @FormUrlEncoded
    Call<ResponseBody> set_rating_workout(@Field("patient_id") Integer patient_id,
                                            @Field("workout_id") Integer workout_id,
                                            @Field("rating") Integer rating);
    //Service rate workout
    @POST("mindsets/rating_mindset/")
    @FormUrlEncoded
    Call<ResponseBody> set_rating_mindset(@Field("patient_id") Integer patient_id,
                                          @Field("mindset_id") Integer mindset_id,
                                          @Field("rating") Integer rating);

    //Service rate recipe
    @POST("foods/rating_recipe/")
    @FormUrlEncoded
    Call<ResponseBody> rating_recipe(@Field("patient_id") Integer patient_id,
                                     @Field("recipe_id") Integer recipe_id,
                                     @Field("rating") Integer rating);


    @GET("programs/get_history_message/{program_id}")
    Call<GetMessages> get_history_message(@Path("program_id") Integer id);

    @GET("generals/get_video_goal/")
    Call<VideoGoal> get_video_goal();

    //Service set goal
    @POST("users/set_goal/")
    @FormUrlEncoded
    Call<ResponseBody> set_goal(@Field("patient_id") Integer patient_id,
                                            @Field("goal") String goal);

    // Service get shipping
    @GET("users/view_shipping_address/{id}")
    Call<GetShippingInfo> get_shipping_info(@Path("id") String id);

    //Service update shipping
    @POST("users/update_shipping_address/")
    @FormUrlEncoded
    Call<ResponseBody> update_shipping_info(@Field("id") String id,
                                          @Field("city") String city,
                                          @Field("address1") String address1,
                                        @Field("address2") String address2,
                                        @Field("phone") String phone,
                                        @Field("postal_code") String postal_code,
                                        @Field("country") String country,
                                        @Field("state") String state);

    @GET("onboard/get_screen/")
    Call<ArrayList<GetLifestyle>> get_lifestyle();

    //Service update password
    @POST("users/update_password/")
    @FormUrlEncoded
    Call<UpdatePass> update_password(@Field("patient_id") Integer patient_id,
                                     @Field("new_password") String new_password,
                                     @Field("old_password") String old_password);


    //Service get imei
    @GET("generals/get_imei/{id}")
    Call<GetImei> get_imei(@Path("id") String id);

    //Service set imei
    @POST("generals/set_imei/")
    @FormUrlEncoded
    Call<ResponseBody> set_imei(@Field("patient_id") Integer patient_id,
                                     @Field("imei") String imei);
    //Set weight
    @POST("users/set_weight/")
    @FormUrlEncoded
    Call<ResponseBody> set_weight(@Field("patient_id") Integer id,
                                  @Field("weight") String weight);

    // Service get single meals
    @GET("foods/today_meals/{patient_id}/{meal_id}/{group}/")
    Call<SingleMeals> get_single_meals(@Path("patient_id") Integer patient_id, @Path("meal_id") String meal_id, @Path("group") String group);

    //Set weight
    @POST("users/register_food_preference/")
    Call<ResponseBody> set_food_preference(@Body Data data);

    // Service do daily check
    @GET("users/do_daily_check/{patient_id}/")
    Call<DoDailyCheck> do_daily_check(@Path("patient_id") Integer patient_id);


    // Service reset password
    @POST("users/reset_password/")
    @FormUrlEncoded
    Call<ResponseBody> reset_password(@Field("mail") String mail);

    //Set lifestyle answers
    @POST("onboard/set_multi_answer/")
    Call<ResponseBody> set_lifestyle_answers(@Body DataLifestyle data);

    // Serviceget equipment
    @GET("workouts/get_equipment/")
    Call<Equipment> get_equipment();

    //Set misc answers
    @POST("users/register_equipment/")
    Call<ResponseBody> set_equipment(@Body DataMiscelaneus data);

    // Service get user profile
    @GET("users/view_profile/{patient_id}/")
    Call<UserProfile> get_user_profile(@Path("patient_id") Integer patient_id);

    //Set update profile
    @POST("users/update_profile/")
    Call<ResponseBody> update_profile(@Body DataEditProfile data);

    // Service view onboarding articles
    @GET("content/view_onboarding_articles/{patient_id}/")
    Call<ViewOnboardingArticles>view_onboarding_articles(@Path("patient_id") Integer patient_id);

    // Service View Single Article
    @GET("content/view_single_article/{article_id}/")
    Call<ViewSingleArticle>view_single_article(@Path("article_id") Integer article_id);

    // Service view daily workouts
    @GET("users/get_daily_check_mindset_work/{patient_id}/")
    Call<DailyWorkouts>get_daily_workouts(@Path("patient_id") Integer patient_id);

    //Set daily workjouts
    @POST("users/set_daily_check_mindset_work/")
    Call<ResponseBody> set_daily_workouts(@Body DataDailyWorkouts data);

    //Service set imei
    @POST("progress/register_progress_felling/")
    Call<ResponseBody> set_daily_feeling(@Body DataFeeling data);


    @Multipart
    @POST("users/upload_avatar/")
    Call<ResponseBody> uploadAvatar(@Part("patient_id") RequestBody patient_id, @Part MultipartBody.Part file);

    // Service Get daily check meals
    @GET("users/get_daily_check_meals/{patient_id}/")
    Call<ArrayList<GetDailyCheckMeal>>get_daily_check_meals(@Path("patient_id") Integer patient_id);

    //Set daily dont follow
    @POST("foods/register_do_not_foods/")
    Call<ResponseBody> set_daily_dont_follow(@Body DataDontFollow data);

    // Service Get daily check meals
    @GET("onboard/get_question_file/")
    Call<LifestyleVideo>get_lifestyle_video();

    //Service set daily meals
    @POST("users/set_daily_check_meals/")
    Call<ResponseBody> set_daily_meals(@Body DataDailyMeals data);

    //Service set goal
    @POST("onboard/set_question_file/")
    @FormUrlEncoded
    Call<ResponseBody> set_question_file(@Field("patient_id") Integer patient_id,
                                         @Field("answer1") String answer1,
                                         @Field("answer2") String answer2);

    //Service set daily check final
    @POST("progress/register_final_information/")
    @FormUrlEncoded
    Call<ResponseBody> register_final_information(@Field("conversation") Integer conversation,
                                                  @Field("take") Integer take,
                                                  @Field("real_reason") Integer real_reason,
                                                  @Field("review_mindset") Integer review_mindset,
                                                  @Field("patient_id") Integer patient_id);

    @GET("users/create_login_register/{pk}/")
    Call<ResponseBody>send_home_access(@Path("pk") Integer pk);

    //Service set goal
    @POST("onboard/skip_video/")
    @FormUrlEncoded
    Call<ResponseBody> skip_video(@Field("patient_id") Integer patient_id);

    //Service skip video
    @POST("generals/register_device/")
    @FormUrlEncoded
    Call<ResponseBody> register_device(@Field("patient_id") Integer patient_id,
                                       @Field("token") String token);

    @GET("invoices/check_available_food_order/{patient_id}")
    Call<ResponseBody>check_available_food_order(@Path("patient_id") Integer patient_id);
}
