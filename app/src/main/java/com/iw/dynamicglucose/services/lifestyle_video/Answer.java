
package com.iw.dynamicglucose.services.lifestyle_video;


public class Answer {

    private String answer;
    private Integer id;

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

}
