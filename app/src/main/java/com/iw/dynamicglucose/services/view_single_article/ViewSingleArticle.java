
package com.iw.dynamicglucose.services.view_single_article;


public class ViewSingleArticle {

    private String subtitle;
    private String title;
    private String media;
    private String image;
    private String content;
    private Boolean realReason;

    public String getSubtitle() {
        return subtitle;
    }

    public void setSubtitle(String subtitle) {
        this.subtitle = subtitle;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getMedia() {
        return media;
    }

    public void setMedia(String media) {
        this.media = media;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Boolean getRealReason() {
        return realReason;
    }

    public void setRealReason(Boolean realReason) {
        this.realReason = realReason;
    }

}
