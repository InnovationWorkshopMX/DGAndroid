package com.iw.dynamicglucose;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.iw.dynamicglucose.services.DailyWorkout.Workout;

import java.util.List;

/**
 * Created by IW on 19/02/2018.
 */

public class AdapterDailyWorkouts extends BaseAdapter {
    private LayoutInflater inflator;
    private List<Workout> answers;

    AdapterDailyWorkouts(Context context, List<Workout> answers){
        inflator = LayoutInflater.from(context);
        this.answers = answers;
    }

    @Override
    public int getCount() {
        return answers.size();
    }

    @Override
    public Object getItem(int i) {
        return answers.get(i);
    }

    @Override
    public long getItemId(int i) {
        return answers.get(i).getId();
    }
    @SuppressLint({"ResourceAsColor", "ViewHolder"})
    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        final Workout answer = answers.get(i);
        View workout_view = inflator.inflate(R.layout.cell_daily_workouts, (ViewGroup) viewGroup.getRootView(), false);
        TextView title = workout_view.findViewById(R.id.txt_cell_daily_workouts);
        title.setText(answer.getName());
        if(answer.getDone()){
            workout_view.setBackgroundColor(Color.parseColor("#FEF1F2"));
            ImageView check = workout_view.findViewById(R.id.daily_check_arrow);
            check.setVisibility(View.VISIBLE);
        }
        return workout_view;
    }
}
