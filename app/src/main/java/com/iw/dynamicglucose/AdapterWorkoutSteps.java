package com.iw.dynamicglucose;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.iw.dynamicglucose.services.process_workout.Step;

import java.util.List;

/**
 * Created by IW on 01/02/2018.
 */

public class AdapterWorkoutSteps extends BaseAdapter{
    private LayoutInflater inflator;
    private Context context;
    private List<Step> steps;
    private String time;

    AdapterWorkoutSteps(Context context, List<Step> steps, String time){
        inflator = LayoutInflater.from(context);
        this.steps = steps;
        this.time = time;
        this.context = context;
    }

    public int getCount(){
        return steps.size();
    }

    public Object getItem(int position){
        return null;
    }

    public long getItemId(int position) {

        return 0;
    }

    @SuppressLint({"ViewHolder", "InflateParams"})
    public View getView(int position, View convertView, ViewGroup parent){
        final Step step = steps.get(position);
        View setps_convertView = inflator.inflate(R.layout.cell_steps_workout, null);
        TextView title = setps_convertView.findViewById(R.id.title_step);
        title.setText(step.getTitle());
        setps_convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            Bundle arguments = new Bundle();
            arguments.putString("title", step.getTitle());
            arguments.putString("media", step.getMedia());
            arguments.putString( "content", step.getStep());
            arguments.putString("time", time);
            fragment_single_workout fragment = fragment_single_workout.newInstance(arguments);
            FragmentTransaction ft = ((FragmentActivity)context).getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.frame_layout, fragment);
            ft.commit();

            }
        });
        return setps_convertView;

    }
}
