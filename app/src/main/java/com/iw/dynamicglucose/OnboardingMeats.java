package com.iw.dynamicglucose;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.animation.PropertyValuesHolder;
import android.animation.ValueAnimator;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Point;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.google.gson.Gson;
import com.iw.dynamicglucose.ExtraObjects.Data;
import com.iw.dynamicglucose.config.Service;
import com.iw.dynamicglucose.config.Session;
import com.iw.dynamicglucose.services.Services;
import com.iw.dynamicglucose.services.applogin.UserSession;
import com.iw.dynamicglucose.services.get_food_items.FoodItem;
import com.yuyakaido.android.cardstackview.CardStackView;
import com.yuyakaido.android.cardstackview.SwipeDirection;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OnboardingMeats extends AppCompatActivity {

    private CardStackView cardStackView;
    private TextView count;
    private Integer flagCount = 1;
    // Declare vars config
    private Service service;
    private Services services;
    private Session session;
    private UserSession user;
    private ImageButton dislike_button;
    private ImageButton alergic_button;
    private ImageButton like_button;
    private ArrayList<Integer> idArray = new ArrayList<Integer>();
    private ArrayList<Integer> empathyArray = new ArrayList<Integer>();
    private ArrayList<Map<String, Integer>> sendInfo = new ArrayList<Map<String, Integer>>();
    private Boolean flagSettings;

    // Declare var progress dialog
    private  MaterialDialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_onboarding_meats);
        getSupportActionBar().hide();
        // Assignment vars config
        service = new Service();
        services = service.getService();
        session = new Session(getBaseContext());
        user = session.getSession();

        flagSettings = getIntent().getBooleanExtra("settings", false);

        count = (TextView)findViewById(R.id.txtCountOnboardMeats);
        cardStackView = (CardStackView)findViewById(R.id.stackViewOnboardMeals);
        dislike_button = (ImageButton)findViewById(R.id.btn_dislike_onboard_meats);
        alergic_button = (ImageButton)findViewById(R.id.btn_alergic_onboard_meats);
        like_button = (ImageButton)findViewById(R.id.btn_like_onboard_meats);
        cardStackView.setLeftOverlay(R.layout.sad_face_tinder);
        cardStackView.setRightOverlay(R.layout.happy_face_tinder);
        cardStackView.setTopOverlay(R.layout.alergic_face_tinder);
        cardStackView.setVisibleCount(2);
        cardStackView.setSwipeDirection(SwipeDirection.FREEDOM_NO_BOTTOM);

//        final AnimatorSet as = new AnimatorSet();

        dislike_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                View target = cardStackView.getTopView();
                View targetOverlay = cardStackView.getTopView().getOverlayContainer();
                ValueAnimator rotation = ObjectAnimator.ofPropertyValuesHolder(
                        target, PropertyValuesHolder.ofFloat("rotation", -10f));
                rotation.setDuration(200);
                ValueAnimator translateX = ObjectAnimator.ofPropertyValuesHolder(
                        target, PropertyValuesHolder.ofFloat("translationX", 0f, -2000f));
                ValueAnimator translateY = ObjectAnimator.ofPropertyValuesHolder(
                        target, PropertyValuesHolder.ofFloat("translationY", 0f, 500f));
                translateX.setStartDelay(100);
                translateY.setStartDelay(100);
                translateX.setDuration(500);
                translateY.setDuration(500);
                AnimatorSet cardAnimationSet = new AnimatorSet();
                cardAnimationSet.playTogether(rotation, translateX, translateY);

                ObjectAnimator overlayAnimator = ObjectAnimator.ofFloat(targetOverlay, "alpha", 0f, 1f);
                overlayAnimator.setDuration(200);
                AnimatorSet overlayAnimationSet = new AnimatorSet();
                overlayAnimationSet.playTogether(overlayAnimator);
                cardStackView.swipe(SwipeDirection.Left, cardAnimationSet);
            }
        });
        alergic_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                View target = cardStackView.getTopView();
                View targetOverlay = cardStackView.getTopView().getOverlayContainer();
                ValueAnimator rotation = ObjectAnimator.ofPropertyValuesHolder(
                        target, PropertyValuesHolder.ofFloat("rotation", -10f));
                rotation.setDuration(200);
                ValueAnimator translateX = ObjectAnimator.ofPropertyValuesHolder(
                        target, PropertyValuesHolder.ofFloat("translationX", 0f, -2000f));
                ValueAnimator translateY = ObjectAnimator.ofPropertyValuesHolder(
                        target, PropertyValuesHolder.ofFloat("translationY", 0f, 500f));
                translateX.setStartDelay(100);
                translateY.setStartDelay(100);
                translateX.setDuration(500);
                translateY.setDuration(500);
                AnimatorSet cardAnimationSet = new AnimatorSet();
                cardAnimationSet.playTogether(rotation, translateX, translateY);

                ObjectAnimator overlayAnimator = ObjectAnimator.ofFloat(targetOverlay, "alpha", 0f, 1f);
                overlayAnimator.setDuration(200);
                AnimatorSet overlayAnimationSet = new AnimatorSet();
                overlayAnimationSet.playTogether(overlayAnimator);
                cardStackView.swipe(SwipeDirection.Top, cardAnimationSet);
            }
        });
        like_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                View target = cardStackView.getTopView();
                View targetOverlay = cardStackView.getTopView().getOverlayContainer();

                ValueAnimator rotation = ObjectAnimator.ofPropertyValuesHolder(
                        target, PropertyValuesHolder.ofFloat("rotation", 10f));
                rotation.setDuration(200);
                ValueAnimator translateX = ObjectAnimator.ofPropertyValuesHolder(
                        target, PropertyValuesHolder.ofFloat("translationX", 0f, 2000f));
                ValueAnimator translateY = ObjectAnimator.ofPropertyValuesHolder(
                        target, PropertyValuesHolder.ofFloat("translationY", 0f, 500f));
                translateX.setStartDelay(100);
                translateY.setStartDelay(100);
                translateX.setDuration(500);
                translateY.setDuration(500);
                AnimatorSet cardAnimationSet = new AnimatorSet();
                cardAnimationSet.playTogether(rotation, translateX, translateY);

                ObjectAnimator overlayAnimator = ObjectAnimator.ofFloat(targetOverlay, "alpha", 0f, 1f);
                overlayAnimator.setDuration(200);
                AnimatorSet overlayAnimationSet = new AnimatorSet();
                overlayAnimationSet.playTogether(overlayAnimator);
                cardStackView.swipe(SwipeDirection.Right, cardAnimationSet);
            }
        });

        cardStackView.setCardEventListener(new CardStackView.CardEventListener() {
            @Override
            public void onCardDragging(float percentX, float percentY) {

            }

            @Override
            public void onCardSwiped(SwipeDirection direction) {
                flagCount++;
                count.setText(flagCount + " of " + idArray.size());
                if(direction.equals(SwipeDirection.Right)){
                    empathyArray.add(1);
                }
                if(direction.equals(SwipeDirection.Left)){
                    empathyArray.add(0);
                }
                if(direction.equals(SwipeDirection.Top)){
                    empathyArray.add(3);
                }
                if(flagCount > idArray.size()){
                    for(int i=0; i<idArray.size();i++){
                        Map<String, Integer> b = new HashMap<String,Integer>();
                        b.put("empathy", empathyArray.get(i));
                        b.put("food_id", idArray.get(i));
                        sendInfo.add(b);
                    }
                    Data d = new Data(user.getId(), 1,sendInfo);
                    setFoodPreference(d);

                }
            }

            @Override
            public void onCardReversed() {

            }

            @Override
            public void onCardMovedToOrigin() {

            }

            @Override
            public void onCardClicked(int index) {

            }
        });
        loadFoods("1");

    }
    private void loadFoods(String id){
        dialog = loader();
        dialog.show();
        // Create the call of the service time to start
        Call<ArrayList<FoodItem>> call = services.get_food_items(id);

        // Executing call of the service time to start
        call.enqueue(new Callback<ArrayList<FoodItem>>() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onResponse(Call<ArrayList<FoodItem>> call, Response<ArrayList<FoodItem>> response) {
                switch (response.code()){
                    case 200:
                        // Getting data objects
                        ArrayList<FoodItem> data = response.body();
                        cardStackView.setVisibleCount(data.size());
                        // Set value to service response to label text days
                        cardStackView.setAdapter(new AdapterOnboardTinder(getBaseContext(), data));
                        for(FoodItem item : data){
                            idArray.add(item.getId());
                        }
                        count.setText(flagCount+ " of " + idArray.size());
                        //cardStackView.setAdapter(new AdapterOnboardTinder(getBaseContext(), data));
                        break;
                    default:

                        break;
                }
                dialog.dismiss();
            }

            @Override
            public void onFailure(Call<ArrayList<FoodItem>> call, Throwable t) {
                Log.e("Error", "call: " + t.getMessage());
                dialog.dismiss();
            }


        });
    }
    private void  setFoodPreference(Data info){
        dialog = loader();
        dialog.show();
        // Create the call of the service view program progress
        Call<ResponseBody> call = services.set_food_preference(info);

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                switch (response.code()) {
                    case 200:
                        if(flagSettings){
                            finish();
                        }else{
                            Intent i = new Intent(getApplicationContext(), OnboardingVegetables.class);
                            startActivity(i);
                            finish();
                        }
                        break;
                    default:
                        break;
                }
                dialog.dismiss();
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                dialog.dismiss();
            }
        });
    }

    private MaterialDialog loader(){
        MaterialDialog.Builder builder = new MaterialDialog.Builder(this)
                .content("Please wait...")
                .progress(true, 0);
        return builder.build();
    }
}
