package com.iw.dynamicglucose;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.RelativeLayout;

import com.afollestad.materialdialogs.MaterialDialog;
import com.iw.dynamicglucose.config.BaseUrl;
import com.iw.dynamicglucose.config.Service;
import com.iw.dynamicglucose.config.Session;
import com.iw.dynamicglucose.services.Services;
import com.iw.dynamicglucose.services.applogin.UserSession;

public class Settings extends AppCompatActivity {

    private RelativeLayout profile;
    private RelativeLayout password;
    private RelativeLayout shipping;
    private RelativeLayout scale;
    private RelativeLayout notifications;
    private RelativeLayout terms;
    private RelativeLayout food;
    private RelativeLayout store;
    private Button logOut;
    private ImageButton back;

    private Session session;
    private UserSession user;
    private BaseUrl baseUrl;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
        setContentView(R.layout.activity_settings);

        // Assignment vars of the view
        profile = findViewById(R.id.item_edit_profile);
        shipping = findViewById(R.id.item_shipping_info);
        scale = findViewById(R.id.item_digital_scale);
        notifications = findViewById(R.id.item_notifications);
        terms = findViewById(R.id.item_terms);
        food = findViewById(R.id.item_update_preferences);
        store = findViewById(R.id.item_store);
        password = findViewById(R.id.item_change_password);
        logOut = findViewById(R.id.btn_logout);
        back = findViewById(R.id.btn_back);

        // Assignment vars objects config
        session = new Session(getApplicationContext());
        user = session.getSession();
        baseUrl = new BaseUrl();

        // Set all listeners
        setListeners();
    }

    private void setListeners() {
        shipping.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getApplicationContext(), ShippingInfo.class);
                startActivity(i);
            }
        });
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getApplicationContext(), EditProfile.class);
                startActivity(i);
            }
        });
        password.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getApplicationContext(), ChangePassword.class);
                startActivity(i);
            }
        });

        terms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getApplicationContext(), Terms.class);
                startActivity(i);
            }
        });

        scale.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getApplicationContext(), ScaleParing.class);
                startActivity(i);
            }
        });

        notifications.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getApplicationContext(), SettingsNotifications.class);
                startActivity(i);
            }
        });

        food.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getApplicationContext(), LifestyleUpdate.class);
                startActivity(i);
            }
        });
        store.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(baseUrl.getBaseurl() + "users/store_users/"+ user.getToken() + "/"));
                startActivity(browserIntent);
            }
        });
        logOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                logOutUser();
            }
        });
    }

    private void logOutUser() {
        if(session.CloseSession() == 1){
            Intent i = new Intent(getApplicationContext(), Login.class);
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(i);
            finish();
        }
    }
}
