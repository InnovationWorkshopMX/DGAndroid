package com.iw.dynamicglucose;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.iw.dynamicglucose.services.get_food_items.FoodItem;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by IW on 02/02/2018.
 */

public class AdapterOnboardTinder extends BaseAdapter {
    private LayoutInflater inflator;
    private Context context;
    private ArrayList<FoodItem> items;

    AdapterOnboardTinder(Context context, ArrayList<FoodItem> items){
        inflator = LayoutInflater.from(context);
        this.items = items;
        this.context = context;
    }

    public int getCount(){
        return items.size();
    }

    public Object getItem(int position){
        return items.get(position);
    }

    public long getItemId(int position) {
        return position;
    }

    @SuppressLint({"ViewHolder", "InflateParams"})
    public View getView(int position, View convertView, ViewGroup parent){
        final FoodItem item = items.get(position);
        View onboard_convertView = inflator.inflate(R.layout.card_tinder, null);
        TextView question = onboard_convertView.findViewById(R.id.txtQuestionTinder);
        ImageView image = onboard_convertView.findViewById(R.id.imgTinder);
        question.setText(item.getQuestion());
        Picasso.with(context).load(item.getPhoto()).into(image);
        return onboard_convertView;

    }
}
