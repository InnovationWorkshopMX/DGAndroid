package com.iw.dynamicglucose;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.RelativeLayout;

public class LifestyleUpdate extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lifestyle_update);
        getSupportActionBar().hide();
        RelativeLayout lifestyle = findViewById(R.id.lifestyle_liestyle_update);
        RelativeLayout foods = findViewById(R.id.food_liestyle_update);
        RelativeLayout misdc = findViewById(R.id.misc_liestyle_update);
        ImageButton close = findViewById(R.id.btn_close_lifestyle_update);

        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        foods.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getApplicationContext(), FoodUpdate.class);
                startActivity(i);
            }
        });
        lifestyle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getApplicationContext(), Lifestyle.class);
                i.putExtra("settings", true);
                startActivity(i);
            }
        });
        misdc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getApplicationContext(), Miscelaneus.class);
                i.putExtra("settings", true);
                startActivity(i);
            }
        });
    }
}
