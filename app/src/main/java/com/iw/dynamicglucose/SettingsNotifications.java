package com.iw.dynamicglucose;

import android.app.Notification;
import android.app.NotificationManager;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.Switch;

import com.iw.dynamicglucose.config.Session;
import com.onesignal.OSSubscriptionObserver;
import com.onesignal.OneSignal;
import com.onesignal.OneSignalDbHelper;
import com.pushbots.push.Pushbots;

import static com.onesignal.OneSignal.getPermissionSubscriptionState;

public class SettingsNotifications extends AppCompatActivity {

    private Switch aswitch;
    private Session session;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings_notifications);
        getSupportActionBar().hide();


        aswitch = findViewById(R.id.switch_notifications);
        ImageButton back = findViewById(R.id.btn_back_notifications);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        session = new Session(getApplicationContext());

        if(session.getstateNotif()){
            aswitch.setChecked(true);
        }else{
            aswitch.setChecked(false);
        }

        aswitch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(aswitch.isChecked()){
                    OneSignal.setSubscription(true);
                    session.setstateNotif(true);

                }else{
                    OneSignal.setSubscription(false);
                    session.setstateNotif(false);
                }
            }
        });
    }
}
