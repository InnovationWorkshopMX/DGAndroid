package com.iw.dynamicglucose;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.iw.dynamicglucose.config.Service;
import com.iw.dynamicglucose.config.Session;
import com.iw.dynamicglucose.services.Services;
import com.iw.dynamicglucose.services.applogin.UserSession;
import com.iw.dynamicglucose.services.digital_pairing.GetImei;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ScaleParing extends AppCompatActivity {
    private Services services;
    private UserSession user;
    private EditText editText;

    //loader
    private MaterialDialog dialogScaling;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scale_paring);
        getSupportActionBar().hide();

        Service service = new Service();
        services = service.getService();
        Session session = new Session(getBaseContext());
        user = session.getSession();

        editText = findViewById(R.id.input_imei);
        Button save = findViewById(R.id.btn_pairing);
        ImageButton back = findViewById(R.id.btn_back_pairing);

        getImei();
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(editText.getText().toString().equals("")){
                    createToast("Please enter de imei");
                }else{
                    setImei(editText.getText().toString());
                }
            }
        });
    }

    private void getImei(){
        dialogScaling = loader();
        dialogScaling.show();
        // Create the call of the service view program progress
        Call<GetImei> call = services.get_imei(user.getId().toString());

        // Executing call of the service view program progress
        call.enqueue(new Callback<GetImei>() {
            @Override
            public void onResponse(Call<GetImei> call, Response<GetImei> response) {
                switch (response.code()) {
                    case 200:
                        GetImei data = response.body();
                        assert data != null;
                        editText.setHint(data.getImei());
                        break;
                    default:

                        break;
                }
                dialogScaling.dismiss();
            }

            @Override
            public void onFailure(Call<GetImei> call, Throwable t) {
                dialogScaling.dismiss();
                createToast("Error: " + t.getMessage());
            }
        });
    }
    private void  setImei(String imei){
        dialogScaling = loader();
        dialogScaling.show();
        // Create the call of the service view program progress
        Call<ResponseBody> call = services.set_imei(user.getId(), imei);

        // Executing call of the service time to start
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                switch (response.code()) {
                    case 200:
                        createToast("Success");
                        finish();
                        break;
                    case 404:
                        createToast("The imei entered is incorrect");
                        break;

                    default:
                       createToast("Server not available, try again later");

                        break;
                }
                dialogScaling.dismiss();
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                createToast("Error" + t);
                dialogScaling.dismiss();
            }
        });
    }
    private void createToast(String msg){
        Toast toast = Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_LONG);
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.show();
    }

    private MaterialDialog loader(){
        MaterialDialog.Builder builder = new MaterialDialog.Builder(this)
                .content("Please wait...")
                .progress(true, 0);
        return builder.build();
    }
}
