package com.iw.dynamicglucose.config;

import com.iw.dynamicglucose.services.Services;
import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by IW on 01/feb/2018.
 */

public class Service {

    private Services services;

    public Service(){
        // Create object BaseUrl
        BaseUrl baseUrl = new BaseUrl();
        // Create object Gson for converter factory
        Gson gson = new GsonBuilder()
                .setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
                .create();
        // Create object retrofit for petition
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(baseUrl.getUrl())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        // Create service from te interface of services
        services = retrofit.create(Services.class);
    }

    public Services getService(){
        return services;
    }

}
