package com.iw.dynamicglucose.config;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;

import com.iw.dynamicglucose.services.applogin.UserSession;

/**
 * Created by IW on 01/feb/2018.
 */

public class Session {

    private UserSession user;
    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;

    private static final String PREFER_NAME = "Dynamic Glucose";
    private static final int PRIVATE_MODE = 0;

    @SuppressLint("CommitPrefEdits")
    public Session(UserSession user, Context context){
        this.user = user;
        sharedPreferences = context.getSharedPreferences(PREFER_NAME, PRIVATE_MODE);
        editor = sharedPreferences.edit();
    }

    @SuppressLint("CommitPrefEdits")
    public Session(Context context){
        sharedPreferences = context.getSharedPreferences(PREFER_NAME, PRIVATE_MODE);
        editor = sharedPreferences.edit();
    }

    public void setUser(UserSession user){
        this.user = user;
    }

    public Integer CreateSession(){
        try{
            editor.putInt("dgProgram", user.getDg_program());
            editor.putInt("messageId", user.getMessage_id());
            editor.putInt("id", user.getId());
            editor.putInt("code", user.getCode());
            editor.putString("email", user.getEmail());
            editor.putString("token", user.getToken());
            editor.putString("avatar", user.getAvatar());
            editor.putString("message", user.getMessage());
            editor.putBoolean("show_mindset", user.getShow_mindsets());
            editor.commit();
            return 1;
        }catch (Exception e){
            return 0;
        }
    }

    public Integer CloseSession(){
        try{
            editor.clear();
            editor.putBoolean("firstTime", false);
            editor.commit();
            return 1;
        }catch (Exception e){
            return 0;
        }
    }

    public void setMessageId(Integer messageId){
        try{
            editor.putInt("messageId", messageId);
            editor.commit();
        }catch (Exception ignored){
        }
    }

    public UserSession getSession(){
        UserSession user = new UserSession();
        user.setDg_program(sharedPreferences.getInt("dgProgram", 0));
        user.setMessage_id(sharedPreferences.getInt("messageId", 0));
        user.setId(sharedPreferences.getInt("id", 0));
        user.setCode(sharedPreferences.getInt("code", 0));
        user.setEmail(sharedPreferences.getString("email", null));
        user.setToken(sharedPreferences.getString("token", null));
        user.setAvatar(sharedPreferences.getString("avatar", null));
        user.setMessage(sharedPreferences.getString("message", null));
        user.setShow_mindsets(sharedPreferences.getBoolean("show_mindset", false));
        return user;
    }

    public boolean isFirstTime(){
        return sharedPreferences.getBoolean("firstTime", true);
    }

    public void setFirstTime(){
        editor.putBoolean("firstTime", false);
        editor.commit();
    }

    public boolean getstateNotif(){
        return sharedPreferences.getBoolean("notif", true);
    }

    public void setstateNotif(boolean val){
        editor.putBoolean("notif", val);
        editor.commit();
    }
    public int getCodeNotif(){
        return sharedPreferences.getInt("codenotif", 0);
    }

    public void setCodeNotif(Integer code){
        editor.putInt("codenotif", code);
        editor.commit();
    }
}
