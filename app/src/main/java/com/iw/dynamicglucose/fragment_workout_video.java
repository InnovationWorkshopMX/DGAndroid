
package com.iw.dynamicglucose;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.annotation.RequiresApi;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.MediaController;
import android.widget.TextView;
import android.widget.VideoView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.halilibo.bettervideoplayer.BetterVideoCallback;
import com.halilibo.bettervideoplayer.BetterVideoPlayer;
import com.iw.dynamicglucose.config.Service;
import com.iw.dynamicglucose.config.Session;
import com.iw.dynamicglucose.services.Services;
import com.iw.dynamicglucose.services.applogin.UserSession;
import com.iw.dynamicglucose.services.view_progress_program.ViewProgramProgress;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class fragment_workout_video extends Fragment implements BetterVideoCallback {

    private TextView txtDays;

    private Services services;
    private Session session;
    private UserSession user;
    private ImageView back_button_meals;
    private Button markasdone;
    private MaterialDialog dialogDays;

    private Integer workout_id;
    private Boolean done;
    private BetterVideoPlayer player;

    public fragment_workout_video() {
        // Required empty public constructor
    }
    public static fragment_workout_video newInstance(Bundle arguments) {
        fragment_workout_video fragment = new fragment_workout_video();
        if(arguments != null){
            fragment.setArguments(arguments);
        }
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_fragment_workout_video, container, false);
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    public void onActivityCreated(Bundle savedInstanceState) {

        super.onActivityCreated(savedInstanceState);
        //VideoView videoView = getView().findViewById(R.id.video_container_single_workout);
        TextView title = getView().findViewById(R.id.txt_title_single_workout);
        TextView time = getView().findViewById(R.id.txt_time_single_workout);
        back_button_meals = getView().findViewById(R.id.back_button_meals);
        markasdone = getView().findViewById(R.id.btn_markDone_step_workout);
        TextView content = getView().findViewById(R.id.txt_content_single_workout);
        player = getView().findViewById(R.id.player);
        player.setCallback(this);
        player.setAutoPlay(true);
        txtDays = getView().findViewById(R.id.lbl_days);
        title.setText(getArguments().getString("title"));
        time.setText(getArguments().getString("time"));
        workout_id = getArguments().getInt("workout_id");
        done = getArguments().getBoolean("done");
        player.setSource(Uri.parse(getArguments().getString("media")));
        content.setText(getArguments().getString("content"));

        if(done){
            markasdone.setBackground(getResources().getDrawable(R.drawable.blue_checked_rounded_button));
            markasdone.setText("DONE");
            markasdone.setTextColor(Color.WHITE);
            markasdone.setEnabled(false);
        }


        // Assignment vars config
        Service service = new Service();
        services = service.getService();
        session = new Session(getContext());
        user = session.getSession();

        if(session.getCodeNotif() != 0){
            session.setCodeNotif(0);
        }

        loadProgramProgress();

        back_button_meals.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                android.support.v4.app.Fragment selectedFragment = null;
                selectedFragment = fragment_workouts.newInstance();
                FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.frame_layout, selectedFragment);
                transaction.commit();
            }
        });

        markasdone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Create the call of the service time to start
                Call<ResponseBody> call = services.set_progress_workout(session.getSession().getId(), workout_id);

                // Executing call of the service time to start
                call.enqueue(new Callback<ResponseBody>() {
                    @SuppressLint("SetTextI18n")
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        switch (response.code()) {
                            case 200:
                                markasdone.setBackground(getResources().getDrawable(R.drawable.blue_checked_rounded_button));
                                markasdone.setTextColor(Color.WHITE);
                                markasdone.setText("DONE");
                                markasdone.setEnabled(false);
                                break;
                            default:
                                break;
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        Log.e("errorCall", t.getMessage());
                    }
                });
            }
        });
    }


    private void loadProgramProgress(){
        dialogDays = loader();
        dialogDays.show();
        // Create the call of the service view program progress
        Call<ViewProgramProgress> call = services.view_program_progress(user.getId());

        // Executing call of the service view program progress
        call.enqueue(new Callback<ViewProgramProgress>() {
            @Override
            public void onResponse(Call<ViewProgramProgress> call, Response<ViewProgramProgress> response) {
                switch (response.code()){
                    case 200:
                        ViewProgramProgress data = response.body();
                        assert data != null;
                        if(data.getIs_active()){
                            txtDays.setText(data.getProgress_program());
                        }else{
                            session.CloseSession();
                            Intent i = new Intent(getContext(), Login.class)
                                    .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                            startActivity(i);
                            getActivity().finish();
                        }

                        break;
                    default:
                        MaterialDialog.Builder dialog = createMessage();
                        dialog.build();
                        dialog.show();
                        break;
                }
                dialogDays.dismiss();
            }

            @Override
            public void onFailure(Call<ViewProgramProgress> call, Throwable t) {
                dialogDays.dismiss();
                MaterialDialog.Builder dialog = createMessage();
                dialog.build();
                dialog.show();
            }
        });
    }

    private MaterialDialog.Builder createMessage(){
        return new MaterialDialog.Builder(getContext())
                .title("Error")
                .content("Server not available, try again later")
                .positiveText("Accept");
    }

    private MaterialDialog loader(){
        MaterialDialog.Builder builder = new MaterialDialog.Builder(getContext())
                .content("Please wait...")
                .progress(true, 0);
        return builder.build();
    }

    @Override
    public void onPause() {
        super.onPause();
        player.pause();
    }

    @Override
    public void onStarted(BetterVideoPlayer player) {

    }

    @Override
    public void onPaused(BetterVideoPlayer player) {

    }

    @Override
    public void onPreparing(BetterVideoPlayer player) {

    }

    @Override
    public void onPrepared(BetterVideoPlayer player) {

    }

    @Override
    public void onBuffering(int percent) {

    }

    @Override
    public void onError(BetterVideoPlayer player, Exception e) {

    }

    @Override
    public void onCompletion(BetterVideoPlayer player) {

    }

    @Override
    public void onToggleControls(BetterVideoPlayer player, boolean isShowing) {

    }
}
